terraform {
	required_providers {
		libvirt = {
			source = "dmacvicar/libvirt"
		}
	}
}

provider "libvirt" {
	uri = "qemu:///session"
}
resource "libvirt_pool" "diamond" {
	name = "diamond"
	type = "dir"
	path = "/tmp/diamond-pool"
}

resource "libvirt_volume" "archcloud" {
	name = "archcloud"
	source = "/mnt/gonzales/scratch/Arch-Linux-x86_64-cloudimg-20211001.0.qcow2"
	pool = libvirt_pool.diamond.name
}

resource "libvirt_volume" "diamond" {
	name = "diamond"
	base_volume_id = libvirt_volume.archcloud.id
	pool = libvirt_pool.diamond.name
	size = 21474836480  # 20 GB
}

resource "libvirt_network" "diamond" {
	name = "diamond"
	mode = "none"
	domain = "diamond.localhost"
	addresses = ["10.15.0.0/24"]
	dns {
		enabled = true
		local_only = true
	}
}

resource "libvirt_domain" "diamond" {
	name = "diamond"
	memory = 8192
	vcpu = 8
	running = true
	cloudinit = libvirt_cloudinit_disk.diamond_init.id
	graphics {
		type = "vnc"
		listen_type = "address"
	}
	disk {
		volume_id = libvirt_volume.diamond.id
		scsi = "true"
	}
	console {
		type = "pty"
		source_path = "/dev/pts/4"
		target_type = "serial"
		target_port = "0"
	}
	network_interface {
		network_id = libvirt_network.diamond.id
		mac = "52:54:00:de:38:29"
	}
	filesystem {
		source = "/mnt/gonzales/vm/diamond/projects"
		target = "projects"
		readonly = false
	}

	filesystem {
		source = "/var/cache/pacman/pkg/"
		target = "pacman-cache"
		readonly = true
	}
	filesystem {
		source = "/var/lib/pacman/sync/"
		target = "pacman-sync"
		readonly = true
	}
	xml {
		xslt = file("evm-jtag.xsl")
	}
}

resource "libvirt_cloudinit_disk" "diamond_init" {
	name = "diamond_init"
	user_data = templatefile("${path.module}/cloud_init.cfg", {
		license_dat64=filebase64("license.dat"),
		ssh_pubkey=file("ssh_pubkey")
	})
	pool = libvirt_pool.diamond.name
}
